#include "stdafx.h"
#include "Plansza.h"

Plansza::Plansza(Gra *a)
: gra(a)
{
	menuPos = 0;
	planszaPos = 0;
	planszaMenuPos = { 0, 0 };
	wybrana = { -1, -1 };
	iloscOpcjiMenu = 2;
	wyb = false;
	zmienStatus(MENU);
	rysujMenu(true);
}


Plansza::~Plansza()
{
}

string Plansza::wczytajPlik(string nazwa)
{
	ifstream plik;
	string odczyt = "";
	string s = "";
	plik.open(nazwa.c_str());

	if (plik.good())
	{
		while (!plik.eof())
		{
			getline(plik, s);
			odczyt += s + "\n";
		}

		plik.close();
	}
	else
		cout << "Nie udało się odczytać pliku!" << nazwa;

	return odczyt;
}

void Plansza::rysujMenu(bool przerysuj = false)
{
	if (przerysuj)
		rysujZnak(0, 0, wczytajPlik("menu.txt"), 7);


	rysujZnak(29, 4, "NOWA GRA", menuPos == 0 ? 13 : 7);
	rysujZnak(29, 5, "AUTORZY", menuPos == 1 ? 13 : 7);
	rysujZnak(29, 6, "WYJSCIE", menuPos == 2 ? 13 : 7);

}

void Plansza::rysujAutorzy(bool przerysuj = false)
{
	if (przerysuj)
		rysujZnak(0, 0, wczytajPlik("autorzy.txt"), 7);
}

template <typename T>
string Plansza::inttostr(T n)
{
	stringstream s;
	s << n;
	return s.str();

}

int Plansza::oznacz(bool &a, int b)
{
	a = true;
	return b;
}

bool Plansza::czyWygrana()
{
	for (int i = TALIA_S1; i < TALIA_S4 + 1; i++)
	if (gra->zwrocTalie(static_cast<RTalie>(i))->index(1)->nazwa != "K")
		return false;

	return true;
}

void Plansza::rysujGre(bool przerysuj = false)
{

	if (przerysuj)
	{
		system("cls");
		rysujZnak(0, 0, wczytajPlik("gra.txt"), 7);
	}

	rysujZnak(16, 2, inttostr(gra->pokazWynik()), 7);//Pokazuje aktualny wynik

	if(czyWygrana())
	{
	zmienStatus(KONIEC);
	system("cls");
	rysujZnak(0,0, wczytajPlik("wygrana.txt"), 7);
	}
	

	//RYSOWANIE KART
	//PIERWSZA KOLUMNA
	for (int i = TALIA_1; i < TALIA_5; i++)
	{

		for (int j = 0; j < gra->zwrocTalie(static_cast<RTalie>(i))->iloscKart(); j++)
		{
			if (gra->zwrocTalie(static_cast<RTalie>(i))->index(j + 1) != NULL)
				rysujKarte(36 - 4 * j, 5 + 5 * i, gra->zwrocTalie(static_cast<RTalie>(i))->index(j + 1)->nazwa, gra->zwrocTalie(static_cast<RTalie>(i))->index(j + 1)->kolor,
				((planszaMenuPos.y == i) && (planszaMenuPos.x == 0) && (j == 0)) ? 14 : (((wybrana.x == 0) && (wybrana.y == i) && (j == 0)) ? 10 : 7));

		}

	}
	//TRZECIA KOLUMNA
	for (int i = TALIA_5; i < TALIA_S1; i++)
	{

		for (int j = 0; j < gra->zwrocTalie(static_cast<RTalie>(i))->iloscKart(); j++)
		{
			if (gra->zwrocTalie(static_cast<RTalie>(i))->index(j + 1) != NULL)
				rysujKarte(50 + 4 * j, 5 + 5 * (TALIA_8 - i), gra->zwrocTalie(static_cast<RTalie>(i))->index(j + 1)->nazwa, gra->zwrocTalie(static_cast<RTalie>(i))->index(j + 1)->kolor,
				((planszaMenuPos.y == TALIA_8 - i) && (planszaMenuPos.x == 2) && (j == 0)) ? 14 : (((wybrana.x == 2) && (wybrana.y == TALIA_8 - i) && (j == 0)) ? 10 : 7));

		}

	}
	//SRODKOWE STOSY
	for (int i = TALIA_S1; i < TALIA_S4 + 1; i++)
	{
		if (gra->zwrocTalie(static_cast<RTalie>(i))->index(1) != NULL)
			rysujKarte(43, 5 * (TALIA_S4 - i + 1), gra->zwrocTalie(static_cast<RTalie>(i))->index(1)->nazwa, gra->zwrocTalie(static_cast<RTalie>(i))->index(1)->kolor,
			((planszaMenuPos.y == TALIA_S4 - i) && (planszaMenuPos.x == 1)) ? 14 : 7);

	}
}

void Plansza::rysujKarte(int x, int y, string text, int text2, int kolor = -1)
{
	rysujZnak(x, y, wczytajPlik("karta.txt"), kolor);
	x += 1;
	y += 1;
	rysujZnak(x, y, text, kolor);
	y += 1;
	char c = text2;
	stringstream ss;
	string s;
	ss << c;
	ss >> s;
	rysujZnak(x, y, s, kolor);


}

void Plansza::zmienStatus(STATUS st)
{
	if (st == MENU)
		rysujMenu(true);

	this->statusPlanszy = st;
}

void Plansza::rysujZnak(int ax, int ay, string text, int kolor = -1)
{
	Punkt p;
	p.x = ax;
	p.y = ay;

	COORD coord = { p.x, p.y - 1 };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

	if (kolor >= 0)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), kolor);
	}

	stringstream str(text);
	string podzial;

	while (getline(str, podzial))
	{
		coord.Y++;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
		cout << podzial;
	}

	if (kolor >= 0)
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0);
}


void Plansza::koniecGry()
{
	zmienStatus(KONIEC);
}

STATUS Plansza::status()
{
	return this->statusPlanszy;
}

bool Plansza::rozneOdStosow(int x, int y)
{
	Punkt tab[4]
	{
		{ 1, 0 },
		{ 1, 1 },
		{ 1, 2 },
		{ 1, 3 }
	};

	for (int i = 0; i < 4; i++)
	if ((x == tab[i].x) && (y == tab[i].y))
		return true;

	return false;
}

void Plansza::klawisze(unsigned char znak)
{
	switch (this->status())
	{
	case MENU:
	{
				 if (znak == 72)
				 {

					 menuPos = menuPos == 0 ? 2 : menuPos - 1;
					 rysujMenu();
				 }
				 else if (znak == 80)
				 {

					 menuPos = menuPos == 2 ? 0 : menuPos + 1;
					 rysujMenu();
				 }
				 else if (znak == 13)
				 {

					 if (menuPos == 0)
					 {
						 rysujZnak(0, 0, "", -1);
						 system("CLS");
						 zmienStatus(GRA);
						 rysujGre(true);
					 }
					 else if (menuPos == 1)
					 {
						 system("CLS");
						 zmienStatus(AUTORZY);
						 rysujAutorzy(true);
					 }
					 else if (menuPos == 2)
					 {
						 system("CLS"); exit(0);
					 }

				 }


				 break;
	}


	case GRA:
	{

				if (znak == 72)//gora
				{
					planszaMenuPos.y = planszaMenuPos.y - 1 < 0 ? 3 : planszaMenuPos.y - 1;
				}
				else if (znak == 80)//dol
				{
					planszaMenuPos.y = planszaMenuPos.y + 1 > 3 ? 0 : planszaMenuPos.y + 1;
				}
				else if (znak == 77)//prawo
				{
					planszaMenuPos.x = planszaMenuPos.x + 1 > 2 ? 0 : planszaMenuPos.x + 1;
				}
				else if (znak == 75)//lewo
				{
					planszaMenuPos.x = planszaMenuPos.x - 1 < 0 ? 2 : planszaMenuPos.x - 1;
				}
				else if (znak == 13)
				{
					if (!wyb || rozneOdStosow(wybrana.x, wybrana.y))
					{
						wybrana.x = planszaMenuPos.x;
						wybrana.y = planszaMenuPos.y;
						wyb = true;
					}
					else
					{
						if ((wybrana.x == planszaMenuPos.x) && (wybrana.y == planszaMenuPos.y))
						{
							wybrana.x = -1;
							wybrana.y = -1;
							wyb = false;
						}
						else
						{

							if (planszaMenuPos.x == 2)
							{
								int a = 7 - planszaMenuPos.y;
								int b = ((wybrana.x == 2) ? 7 - wybrana.y : wybrana.y);
								if (gra->mniejszaWieksza(gra->zwrocTalie(static_cast<RTalie>(a))->index(1)->nazwa, gra->zwrocTalie(static_cast<RTalie>(b))->index(1)->nazwa))
									gra->zwrocTalie(static_cast<RTalie>(a))->dodajNaPoczatek(gra->zwrocTalie(static_cast<RTalie>(b))->wysunKarte());
								else
									return;
							}
							else if (planszaMenuPos.x == 0)
							{
								int a = planszaMenuPos.y;
								int b = ((wybrana.x == 2) ? 7 - wybrana.y : wybrana.y);
								if (gra->mniejszaWieksza(gra->zwrocTalie(static_cast<RTalie>(a))->index(1)->nazwa, gra->zwrocTalie(static_cast<RTalie>(b))->index(1)->nazwa))
									gra->zwrocTalie(static_cast<RTalie>(planszaMenuPos.y))->dodajNaPoczatek(gra->zwrocTalie(static_cast<RTalie>((wybrana.x == 2) ? 7 - wybrana.y : wybrana.y))->wysunKarte());
								else
									return;
							}
							else if (planszaMenuPos.x == 1)
							{
								int a = 11 - planszaMenuPos.y;
								int b = ((wybrana.x == 2) ? 7 - wybrana.y : wybrana.y);
								if ((gra->wieksza(gra->zwrocTalie(static_cast<RTalie>(a))->index(1)->nazwa, gra->zwrocTalie(static_cast<RTalie>(b))->index(1)->nazwa)) &&
									gra->zwrocTalie(static_cast<RTalie>(a))->index(1)->kolor == gra->zwrocTalie(static_cast<RTalie>(b))->index(1)->kolor)
									gra->zwrocTalie(static_cast<RTalie>(a))->dodajNaPoczatek(gra->zwrocTalie(static_cast<RTalie>((wybrana.x == 2) ? 7 - wybrana.y : wybrana.y))->wysunKarte());
								else
									return;

							}
							wyb = false;
							wybrana.x = -1;
							wybrana.y = -1;
							gra->dodajPunkty(100);
							rysujGre(true);
						}

					}

				}
				else if (znak == 9)
				{
					gra->resetujGre();
				}
				rysujGre();
				break;
	}

	case KONIEC:
	{
				   system("CLS"); exit(0);

				   break;
	}
	case AUTORZY:
	{

					if (znak == 13)
					{
						system("CLS");  zmienStatus(MENU); rysujMenu(true);
					}
					break;
	}

	}

}

